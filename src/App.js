import React from 'react';
import './App.css'
import './css/reset.css'
import './css/style.css'
import Form from './components/Form'
import About  from './components/About';
import Home from './components/Home';
import Navbar from './components/Navbar';
import Footer  from './components/Footer';
import { Routes, Route } from 'react-router-dom';


class App extends React.Component {
// eslint-disable-next-line    
   constructor(props) {
    super(props)
  
  }
  render() {
    return <div className="container">
      <Navbar/>
      <Routes>
      <Route path='/' element={<Home/>}></Route>
      <Route path='/about' element={<About/>}></Route>
      <Route path='/signup' element={<Form/>}></Route>
      </Routes>
      <Footer />

      </div>
  }
}

export default App;
