import React, { Component } from 'react'
import Banner from '../images/banner2.jpg'


class Home extends Component {
    render() {
        return (
            <div className='container'>
                <div className="w-100">
                    <img src={Banner} alt="banner images" className="w-100"/>
                </div>
                <div className="row">
                    <div className="col-sm-8 ">
                        <div className="card  text-center mt-3 mb-3 p-2">
                            <h1 className='text-primary h3'>What We do</h1>
                        <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more 
                        recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                        </div>
                        <div className="card  text-center mt-3 mb-3 p-2">
                            <h1 className='text-primary h3'>Our Goal</h1>
                        <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more 
                        recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                        </div>
                        
                       
                    </div>
                    <div className="col-sm-4 ">
                        <aside className='card  text-center mt-3 mb-3 p-2'>
                            <div>
                                <h1 class="h3 text-info">Notification</h1>
                            </div>
                            <div>
                                    <ul>
                                        <li>First notification</li>
                                        <li>Second notification</li>
                                        <li>Third notification</li>
                                        <li>Fourth notification</li>
                                    </ul>
                            </div>
                            
                        </aside>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home;
