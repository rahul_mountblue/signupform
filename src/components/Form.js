import React, { Component } from 'react'
import validator from 'validator';
import SignupSuccess from './SignupSuccess';
import SignupImage from '../images/signup.webp';

class Form extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: '',
            email: '',
            age: '',
            password: '',
            cpassword: '',
            termsaccepted: false,
            errors: {},
            isSubmit: false,
            showError: true,

        }
    }

    handleChange = (event) => {

        const { name, value } = event.target;
        this.setState({ showError: false })

        if (event.target.type !== 'checkbox') {
            this.setState({ [name]: value });
        } else {
            this.setState({ [name]: event.target.checked })
        }

    }


    formValidation = () => {
        const { name, email, age, password, cpassword, termsaccepted } = this.state;
        let isValid = true;

        let errors = {
            nameError: '',
            emailError: '',
            ageError: '',
            passError: '',
            cpassError: '',
            matchPassword: '',
            termsAcceptedError: ''

        };

        if (validator.isEmpty(name)) {
            errors.nameError = 'Name can not be empty';
        } else if (validator.isAlpha(name, 'en-US', { ignore: ' -' }) === false) {
            errors.nameError = 'Name can not be invalid value';
        } else if (name.length < 3 || name.length > 30) {
            errors.nameError = 'Name should be between 3 to 30 charactore';
        }

        if (validator.isEmpty(email)) {
            errors.emailError = 'Email can not be empty';
        } else if (validator.isEmail(email, { blacklisted_chars: ' ' }) === false) {
            errors.emailError = 'Invalid mail';
        }

        if (validator.isEmpty(age)) {
            errors.ageError = 'Age can not be empty';
        } else if (validator.isInt(age, { min: 1, max: 150 }) === false) {
            errors.ageError = 'Invalid age, Age should be between 1 to 150';
        }


        let passwordParam = {
            minLength: 4, maxLength: 10, minLowercase: 1,
            minUppercase: 1, minNumbers: 1,
            minSymbols: 1, returnScore: false
        }

        if (validator.isEmpty(password)) {
            errors.passError = 'Password field can not be empty';
        } else if (validator.isStrongPassword(password, passwordParam) === false) {
            errors.passError = `Need 1 lowercase, 
            1 uppercase alphabet and 1 number and 1 special symbol`;
        }

        if (validator.isEmpty(cpassword)) {
            errors.cpassError = 'Confirm Password field can not be empty';
        } else if (validator.isStrongPassword(cpassword, passwordParam) === false) {
            errors.cpassError = `Need 1 lowercase, 
            1 uppercase alphabet and 1 number and 1 special symbol`;
        }

        if (validator.equals(password, cpassword) === false) {
            errors.matchPassword = "Password not matched"
        }

        if (termsaccepted === false) {
            errors.termsAcceptedError = "Please accept the terms and conditions"
        }


        this.setState({ errors });

        let isError = Object.keys(errors).find(error => {
            return errors[error].length > 0
        })

        if (isError !== undefined) {
            isValid = false;
        }
        return isValid;


    }

    handleSubmit = (event) => {
        event.preventDefault();

        let validSubmission = this.formValidation();

        this.setState({
            isSubmit: validSubmission,
            showError: true,
        });
        if (validSubmission) {
            console.log(this.state)
            
        }
    }

    shouldComponentUpdate(prevProps, prevState) {
        console.log('inside shouldComponent update');
        if (prevState.errors !== this.state.errors || 
            prevState.showError !== this.state.showError 
            || prevState.isSubmit != this.state.isSubmit) {
            return true;
        }
        return false;
    }

    refreshPage =()=>{
        console.log('refresh');
        this.setState({
                name: '', email: '', age: '', password: '', cpassword: '', termsAcceptedError: false,error:{}
            })
        this.setState({isSubmit:false});
    }

    render() {
        const { name, email, age, password, cpassword, errors, isSubmit, showError } = this.state;
        console.log(isSubmit);
        console.log(this.state)
        return (

            <div className="container-fluid">
                {
                    !isSubmit ?
                        <div className="row justify-content-center">
                            
                            <div className="col-12 col-sm-6 col-md-4">
                                <form className="form-container card p-3 mt-3 mb-3" action="" onSubmit={this.handleSubmit}>
                                    <div className="mb-3">
                                        <h1 className="text-center text-primary">Create Account</h1>
                                    </div>

                                    <div className="mb-3">

                                        <input type="text"
                                            name="name"
                                            className="form-control"
                                            id="nameInput"
                                            onChange={this.handleChange}
                                            defaultValue={name}
                                            placeholder="Enter name"
                                        />

                                        <p className="text-danger text-center mt-1">
                                            {showError && errors.nameError && errors.nameError.length > 0 ?
                                                errors.nameError
                                                :
                                                undefined
                                            }
                                        </p>
                                    </div>

                                    <div className="mb-3">

                                        <input type="email"
                                            name="email"
                                            className="form-control"
                                            id="emailInput"
                                            onChange={this.handleChange}
                                            defaultValue={email}
                                            placeholder="Enter email"
                                        />
                                        <p className="text-danger text-center mt-1">
                                            {showError && errors.emailError && errors.emailError.length > 0 ?
                                                errors.emailError
                                                :
                                                undefined
                                            }
                                        </p>
                                    </div>

                                    <div className="mb-3">
                                        {/* <label htmlFor="ageInput" class="form-label">Name</label> */}
                                        <input type="number"
                                            name="age"
                                            className="form-control"
                                            id="ageInput"
                                            onChange={this.handleChange}
                                            defaultValue={age}
                                            placeholder="Enter age"
                                        />
                                        <p className="text-danger text-center mt-1">
                                            {showError && errors.ageError && errors.ageError.length > 0 ?
                                                errors.ageError
                                                :
                                                undefined
                                            }
                                        </p>
                                    </div>

                                    <div className="mb-3">
                                        
                                        <input type="password"
                                            name="password"
                                            className="form-control"
                                            id="passInput"
                                            onChange={this.handleChange}
                                            defaultValue={password}
                                            placeholder="Enter password"
                                        />

                                        <p className="text-danger text-center mt-1">
                                            {showError && errors.passError && errors.passError.length > 0 ?
                                                errors.passError
                                                :
                                                undefined
                                            }
                                        </p>
                                    </div>

                                    <div className="mb-3">
                                       
                                        <input type="password"
                                            className="form-control"
                                            name="cpassword"
                                            id="confirmPass"
                                            onChange={this.handleChange}
                                            defaultValue={cpassword}
                                            placeholder="Confirm password"
                                        />

                                        <p className="text-danger text-center mt-1">
                                            {showError && errors.cpassError && errors.cpassError.length > 0 ?
                                                errors.cpassError
                                                :
                                                undefined
                                            }
                                            {showError && errors.cpassError && errors.cpassError.length > 0 ? undefined : errors.matchPassword && errors.matchPassword.length > 0 ?
                                                errors.matchPassword
                                                :
                                                undefined
                                            }

                                        </p>

                                    </div>

                                    <div className="form-check">
                                        <input
                                            name="termsaccepted"
                                            className="form-check-input"
                                            type="checkbox"
                                            onChange={this.handleChange}
                                            value="" id="flexCheckChecked"
                                        />
                                        <label className="form-check-label" for="flexCheckChecked">
                                            I accept the Terms and Conditions
                                        </label>
                                        <p className="text-danger text-center mb-2 mt-1">
                                            {showError && errors.termsAcceptedError && errors.termsAcceptedError.length > 0 ?
                                                errors.termsAcceptedError
                                                :
                                                undefined
                                            }
                                        </p>
                                    </div>


                                    <div className="mb-3">
                                        <button type="button" className="btn btn-primary col-11 col-sm-4 col-md-12"
                                            onClick={this.handleSubmit}>
                                            Signup
                                        </button>
                                    </div>
                                    <div className="mb-3">
                                        <p className='text-center text-secondary'>-------Signin with------</p>
                                    </div>
                                    <div className='container d-flex justify-content-around'>
                                        
                                            <i className="fa fa-facebook-f" style={{ fontSize: "30px", color: "blue" }}></i>
                                            <i className="fa fa-google" style={{ fontSize: "30px", color: "red" }}></i>
                                            <i class="fa fa-linkedin" style={{ fontSize: "30px", color: "blue" }}></i>

                                    </div>

                                </form>


                            </div>
                        </div>

                        :
                        <SignupSuccess name={name} email={email} age={age} onClick={this.refreshPage}/>
                }

          </div>
        )
    }
}

export default Form;