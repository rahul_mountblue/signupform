import React, { Component } from 'react'

export default class SignupSuccess extends Component {
    constructor(props) {
        super(props)
        console.log(this.props.refreshPage);

    }
    render() {
        return (
            <div className="container d-flex justify-content-center w-80 mt-5 mb-5 h-25">
                <div className='w-50 card text-center'>
                <h1 className='text-success'>Signup completed</h1>
                <p >Your Name: <span>{this.props.name}</span> </p>
                <p className="">Your Email: <span> {this.props.email}</span> </p>
                <p className="" >Your Age: <span>{this.props.age}</span></p>
                <button className="btn btn-success" onClick={this.props.onClick}>close</button>
                </div>
                
            </div>
        )
    }
}
