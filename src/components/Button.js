import React, { Component } from 'react'

class Button extends React.Component {
    render() {
        return (
            <div>
                <button type="button" class="btn btn-primary" >Submit</button>
            </div>
        )
    }
}
export default Button;
