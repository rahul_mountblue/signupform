import React, { Component } from 'react'
import { Link } from 'react-router-dom';

class Navbar extends Component {
    render() {
        return (
            <div className="container">

                <nav class="navbar navbar-expand-lg navbar-light bg-info">
                    <div class="container-fluid">
                        <div class="navbar " id="navbarNav">

                            <Link to="/" className="nav-link active text-light">Home</Link>
                            <Link to="/about" className="nav-link active text-light">About</Link>
                            <Link to="/signup" className="nav-link active text-light">Signup</Link>


                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}

export default Navbar;
