import React, { Component } from 'react'

class About extends Component {
    render() {
        return (
            <div className="card  text-center mt-3 mb-3 p-2">
                <div>
                    <h1 className='text-primary h3'>About</h1>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of
                        Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                        like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                </div>
            </div>
        )
    }
}
export default  About;