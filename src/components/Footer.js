import React, { Component } from 'react'

class Footer extends Component {
    render() {
        return (
            <div className="container">
                <nav className="navbar  navbar-light bg-dark p-3 ">
                    <p className=' text-light text-center w-100'>This is a footer section</p>
                </nav>
            </div>
        )
    }
}

export default Footer;
